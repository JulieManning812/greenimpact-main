#!/usr/bin/python
from influxdb import InfluxDBClient, exceptions
import requests
import datetime
import time
import urllib
import random

USER = 'root'
PASSWORD = 'root'

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def checkConnectivity(reference):
    print("[+] Checking HTTP API connection")
    request = requests.get(reference)
    status = request.status_code
    print("Status code : " + str(status))
    if status == 200:
        print(OKGREEN + "[OK] Test passed" + ENDC)
    else:
        print(FAIL + "[NOP] FAIL, no connectivity" + ENDC)

def checkReading(client, database, measurement, time, reading):
    print("[+] Checking reading on database")

    try:
        result = client.query('SELECT * FROM ' + measurement + ' WHERE time=' + time)
    except exceptions.InfluxDBClientError:
        print(WARNING + "[ERROR] Cannot query value" + ENDC)

    toCheck = result[measurement].next()
    toCheckTimestamp = toCheck['time']
    toCheckValue = toCheck['value']

    #TODO: Debug
    print(toCheckTimestamp + ' and ' + time)
    print(toCheckValue + ' and ' + reading)

    ok = toCheckTimestamp+'Z'==time and toCheckValue==reading

    if ok:
        print(OKGREEN + "[OK] Test passed" + ENDC)
    else:
        print(FAIL + "[NOP] FAIL, can't read database" + ENDC)

def checkWriting(client, database):
    print("[+] Checking writing on database")

    #Random timestamp between UNIX epoch time and now
    timestamp = random.randint(0, int(time.time()))
    #Random value to give between 0 and 1337
    value = random.randint(0, 1337)
    regtime = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S')
    #Parse the weather id
    j = [{
        "measurement": 'cakeIsALie',
        "time": regtime,
        "fields": {
            "value": value
        }}]

    ok = client.write_points(j)

    if ok:
        print(OKGREEN + "[OK] Test passed" + ENDC)
    else:
        print(FAIL + "[NOP] FAIL, can't write on database" + ENDC)

    return (regtime, value)

def checkCreateDb(client, database):
    print("[+] Checking database creation")

    try:
        client.create_database(database)
        print(OKGREEN + "[OK] Test passed" + ENDC)
    except InfluxDBClientError:
        print(FAIL + "[NOP] FAIL, can't create database " + database + ENDC)

def checkDropDb(client):
    print("[+] Checking database dropping")

    try:
        client.drop_database(database)
        print(OKGREEN + "[OK] Test passed" + ENDC)
    except InfluxDBClientError:
        print(FAIL + "[NOP] FAIL, can't drop database " + database + ENDC)

def checkAddUser(client):
    print("[+] Checking user adding")

    try:
        client.create_user("kikou", "lol")
        print(OKGREEN + "[OK] Test passed" + ENDC)
    except InfluxDBClientError:
        print(FAIL + "[NOP] FAIL, can't add user on database" + ENDC)

def checkDropUser(client):
    print("[+] Checking user removal")

    try:
        client.drop_user("kikou", "lol")
        print(OKGREEN + "[OK] Test passed" + ENDC)
    except InfluxDBClientError:
        print(FAIL + "[NOP] FAIL, can't add remove on database" + ENDC)

def main(host='influxdb', port=8086):
    """
    main function
    """
    db = "stillalive"
    checkConnectivity("influxdb:8083")
    client = InfluxDBClient('influxdb', 8086, USER, PASSWORD, db)
    checkAddUser()
    checkDropUser()
    checkCreateDb(db)
    r = checkWriting(db)
    checkReading(db, 'cakeIsALie',  r[0], r[1])
    checkDropDb(db)

if __name__ == '__main__':
    main()
