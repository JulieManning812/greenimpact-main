# green imPACT

## Description

Our project aims at creating a connected greenhouse. It will operate in a rather simple way: the connected greenhouse will cater to your plants basic needs in water, temperature and humidity. The user will receive regular feedback on the plants state through an app, a website and a graphic interface.

Weather you wish to grow flowers to decorate your private space or vegetables to cook your meals, the connected greenhouse will assist you and make sure your plants won't wither. We strongly believe that this concept in an innovating project that fits into the house and even into the city of the future, both ecological and connected.

## Installation

Just run `./install.sh` and enjoy! 
The script will propose to install all the necessary softwares, download a developpement environnement, pull the configuration files and default parameters, and install the System D services for auto launching at next reboot. 

## Usage

There is no usage for now.

## Repository structure

The chosen structure is a progressive-stability branching. 

 - Production code is on `master`
 - Development code is on `develop`
 - Last code is on `topic`
 - Documents are in `documents`

## Master branch folder structure

`Master` branch contains the production code and is structured as the following: 

 - InfluxDB: Time-Series Database
	 - config contains the config file. 
	 - data contains a template of database. 
 - Mechanical Drawings: contains a 3D model of the greenhouse. WARNING: not updated, WIP. 
 - SystemDServices: contains every System D file to use. 
 - TomCat: contains every piece used for REST API (powered by TomCat). 
 - UnitTests: contains all the scripts used for unit testing. 
 - 3D: contains 3D printed parts used for the product.
 - Android: Android code. 

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

2015/12/05: Initial commit
2015/03/01: Complete restructuration of the repository

## Credits

Authors are listed here :

 - BARAKAT Anas
 - BLANCHET Alexis
 - BOUGHALEM Dounia
 - CHASSETUILLIER Léon
 - DECOODT Clement
 - GILLIOT Maxence
 - KACHOURI Meriem
 - SEVIN Adrien

## License

See LICENSE file

Copyright 2015 green ImPACT project founders

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=======
# GreenImPACT
