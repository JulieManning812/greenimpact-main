package greenimpact.smartgreenhouse;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;

public class FirstPlant extends Activity {
    Button tempButton = null;
    private View.OnClickListener tempButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent secondActivity = new Intent(FirstPlant.this, TemperatureActivity.class);
            startActivity(secondActivity);
        };
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstplant);
        tempButton = (Button) findViewById(R.id.button3);
        tempButton.setOnClickListener(tempButtonListener);
    }
}
