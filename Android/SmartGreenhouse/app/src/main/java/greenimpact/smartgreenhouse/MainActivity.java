
package greenimpact.smartgreenhouse;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {

    Button tempButton1 = null;
    Button tempButton2 = null;
    private View.OnClickListener tempButton2Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent secondActivity = new Intent(MainActivity.this, FirstPlant.class);
            startActivity(secondActivity);

        }
    };
    private View.OnClickListener tempButton1Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent secondActivity = new Intent(MainActivity.this, SecondPlant.class);
            startActivity(secondActivity);

        }
    };
    private ImageView ImgVw = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tempButton1 = (Button) findViewById(R.id.button);
        tempButton1.setOnClickListener(tempButton1Listener);
        tempButton2 = (Button) findViewById(R.id.button2);
        tempButton2.setOnClickListener(tempButton2Listener);
        ImageView imgVw = (ImageView)findViewById(R.id.imageView);
        imgVw.setImageResource(R.drawable.mon_image);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    public void accessEmail(View v)
    {
            /* Create the Intent */
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
/* Fill it with Data */
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"to@email.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");
/* Send it off to the Activity-Chooser */
        this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
}
