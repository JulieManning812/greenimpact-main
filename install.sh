#!/bin/bash

clear
cat << "EOF"

 ,>   )\ `a_                                            _a' /(
(  _  )/ /{_ ~~    GreenImPACT installation script   ~~ _}\ \(  -
 `(,)_,)/                                                  \(,_(,\\
  ,<_ ,<_.                                                 ._>, _>,``==>

EOF

if [ "$(id -u)" != "0" ]
then
	echo "[!] Sorry, you must be root to use this post installation script"
	exit 1
fi

echo "[+] Installing softwares"
echo "[+] Please choose in the following categories"

whiptail --nocancel --separate-output \
	--title "Post-installer" --ok-button "Install" \
	--checklist "Please pick one" \
	$(($(tput lines) * 2 / 3)) $(($(tput cols) * 2 / 3)) $(($(tput lines) / 2)) \
	aptitude "better than default apt-get" on \
	ctags "needed for custom vimrc" on \
	htop "top with graphing support" on \
	tmux "screen-like terminal multiplexer" on  \
	tty-clock "ascii clock" on\
	docker.io "docker runtime environnement" on\
	vim "the best text editor ever" on 2>choices 

TOINSTALL=()

while read choice
do
	TOINSTALL+=($choice)
done < choices

rm choices

echo "[-] selected products: ${TOINSTALL[*]}"

apt-get install ${TOINSTALL[*]} 

echo "[+] Downloading vimrc files"
git clone https://github.com/didjcodt/vimrc.git ~/.vim_runtime
echo "[+] Installing vimrc"
sh ~/.vim_runtime/install_awesome_vimrc.sh

echo "[+] Adding custom vimrc parameters"
cp my_configs.vim ~/.vim_runtime/

echo "[+] Adding .tmux.conf"
cp .tmux.conf ~/

echo "[+] Pulling InfluxDB DockerFile"
docker pull influxdb

echo "[+] Getting default InfluxDB template"
mkdir /opt/InfluxDB
cp -r InfluxDB/config /opt/InfluxDB/
cp -r InfluxDB/data /opt/InfluxDB/

echo "[+] Pulling TomCat DockerFile"
docker pull tomcat

echo "[+] Installing TomCat application"
mkdir /opt/TomCat
cp -r TomCat/ /opt/TomCat
docker build -t greenimpact/greentomcat:0.1 /opt/TomCat

echo "[+] Getting default parameters"
cp config.json /opt/

echo "[+] Pulling java DockerFile"
docker pull java

echo "[+] Getting java apps"
mkdir /opt/JavaApps
cp -r JavaApps/* /opt/JavaApps
docker build -t greenimpact/greenjava:0.1 /opt/JavaApps

if [ -f id_rsa.pub ];
then
	echo "[!] id_rsa.pub detected!"
	read -p "[+] Do you want to add your public key? [Y/n] " -n 1 -r
	echo    # move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		echo "[+] Adding id_rsa.pub in ~/.ssh/"
		cat id_rsa.pub >> ~/.ssh/authorized_keys
	fi
fi

